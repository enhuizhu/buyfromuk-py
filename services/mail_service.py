import requests
import json

def send(subject, content, toEmail, toName):
  url = "https://api.pepipost.com/v5/mail/send"
  headers = {
    'api_key': "f053311d3421c4f07842908bf219f4a7",
    'content-type': "application/json"
  }
  payloadDic ={
    "from": {
      "email": "no-reply@mail.uk-haitao.com",
      "name": "Uk Haitao"
    },
    "subject": subject,
    "content": [
      {
        "type": "html",
        "value": content
      }
    ],
    "personalizations": [
      {
        "to": [
            {
              "email": toEmail,
              "name": toName
            }
        ]
      }
    ]
  }

  return requests.request("POST", url, data=json.dumps(payloadDic), headers=headers)
  