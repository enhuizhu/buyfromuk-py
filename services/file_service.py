import os

def loadContentFromFile(filePath):
  f = open(filePath, 'r')
  return f.read()

def loadForgetPasswordTemplate():
  filePath = os.getcwd() + "/mail_template/forget_password.html"
  return loadContentFromFile(filePath)
