from django.http import HttpResponse
from django.template.loader import get_template
from products.models import Product
from buyfromuk.settings import MEDIA_URL

def index(request):
  products = Product.objects.all()
  vars(products)
  t = get_template('index.html')
  html = t.render({
    'products': products,
    'mediaUrl': MEDIA_URL
  })
  return HttpResponse(html)