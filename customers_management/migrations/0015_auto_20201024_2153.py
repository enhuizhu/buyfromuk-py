# Generated by Django 3.1.1 on 2020-10-24 21:53

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('customers_management', '0014_auto_20201024_2020'),
    ]

    operations = [
        migrations.AddField(
            model_name='topuphistory',
            name='note',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='addressbook',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 10, 24, 21, 53, 3, 37352, tzinfo=utc), verbose_name='date created'),
        ),
        migrations.AlterField(
            model_name='topuphistory',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 10, 24, 21, 53, 3, 40490, tzinfo=utc), verbose_name='date created'),
        ),
    ]
