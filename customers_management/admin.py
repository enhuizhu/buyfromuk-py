from django.contrib import admin
from django.http import HttpResponse
from django.conf.urls import url
from django.urls import path
from django.utils.html import format_html

from .models import Customers, addressBook

class AddressBookInline(admin.StackedInline):
  model = addressBook
  extra = 0

class CustomersAdmin(admin.ModelAdmin):
    inlines = [AddressBookInline]
    search_fields = ('username', 'firstName', 'lastName', 'email')
    list_display = ('username', 'firstName', 'lastName', 'email', 'rmb_balance', 'balance', 'topUpBalance')
    readonly_fields = ['balance', 'rmb_balance']
    
    def topUpBalance(self, obj):
      return format_html(f'<div><input type="button" class="button" style="padding: 5px;" onClick="window.open(\'/customers_management/topup/?id={obj.id}\')" value="Top Up Balance"/></div>')
    # list_filter = ('username', 'firstName',)
    # change_list_template = 'admin/customers/customers_change_list.html'

    # def changelist_view(self, request, extra_context=None):
    #     extra_context = extra_context or {}
    #     extra_context['customers'] = Customers.objects.all()
    #     return super(CustomersAdmin, self).changelist_view(request, extra_context)
      
    # def custom_admin_view(self, request):
    #     return HttpResponse("hello")

    # def get_urls(self):
    #     additional_urls = [
    #         path('test', self.admin_site.admin_view(self.custom_admin_view), name='custom')
    #     ]
    #     # append your custom URL BEFORE default ones
    #     return additional_urls + super().get_urls()

admin.site.register(Customers, CustomersAdmin)
