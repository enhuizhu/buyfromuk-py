from django.db import models
from django.utils import timezone

CURRENCIES = (
  ('0', 'GBP'),
  ('1', 'RMB'),
)

# Create your models here.
class Customers(models.Model):
  username = models.CharField(max_length=200)
  firstName = models.CharField(max_length=200)
  lastName = models.CharField(max_length=200)
  password = models.CharField(max_length=200)
  email = models.CharField(max_length=200)
  mobile = models.CharField(max_length=50, default='')
  balance = models.DecimalField(max_digits=9, decimal_places=2, default=0)
  rmb_balance = models.DecimalField(max_digits=9, decimal_places=2, default=0)
  created_date = models.DateTimeField('date created', default=timezone.now)
  
  def __str__(self):
    return self.username

class addressBook(models.Model):
  customer = models.ForeignKey(to=Customers, related_name='address_book', on_delete=models.CASCADE)
  name = models.CharField(max_length=200)
  mobileNumber = models.CharField(max_length=200)
  area = models.CharField(max_length=200)
  address = models.CharField(max_length=200)
  personalId = models.CharField(max_length=200)
  created_date = models.DateTimeField('date created', default=timezone.now)

class topUpHistory(models.Model):
  customer = models.ForeignKey(to=Customers, related_name='top_up_history', on_delete=models.CASCADE)
  value = models.DecimalField(max_digits=9, decimal_places=2, default=0)
  note = models.TextField(default='')
  currency = models.CharField(max_length=1, choices=CURRENCIES, default='0')
  balance = models.DecimalField(max_digits=9, decimal_places=2, default=0)
  createdBy = models.CharField(max_length=200)
  created_date = models.DateTimeField('date created', default=timezone.now)

# class forgetPassword(models.Model):
#   customer = models.ForeignKey(to=Customers, related_name='forget_password', on_delete=models.CASCADE)
#   expire_date = models.DateTimeField('expire date', null=True, default=None)
#   created_date = models.DateTimeField('date created', default=timezone.now)
