from django.urls import path
from . import views

urlpatterns = [
  path('topup/', views.topup, name='topup')
]
