from customers_management.views import addMoneyToBalance
from api.views.stripePayment import cancelPayment


def refundfromBalance(order, username):
  addMoneyToBalance(order.customer, order.totalFees, 'refund', order.currency, username)

def refundfromStrip(order, username):
  cancelPayment(order)
  addMoneyToBalance(order.customer, order.totalFees, 'refund', order.currency, username)

def refundfromAlipay():
  pass

def refundfromWechatPay():
  pass