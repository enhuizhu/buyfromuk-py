from django.test import TestCase
from .CustomerTokeService import (
  generateJwt, 
  decodeJwt, 
  isTokenExpired, 
  secret, 
  algorithm,
  generateForgetPasswordToken
)
import datetime
import jwt

class CustomerTokenServiceTestCase(TestCase):
  def testGenerateJwt(self):
    result = generateJwt('test')
    print('token result:', result)
    # self.assertEqual(result, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6InRlc3QiLCJleHBpcmVzIjoxNTk5OTQyODM5LjB9.0WSt_NhlD7olm7hFFAnHZcV-FMLCJti9CYdOR2zHZ7M')

  def testDecodeJwt(self):
    result = decodeJwt('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6InRlc3QiLCJleHBpcmVzIjoxNTk5OTQyODM5LjB9.0WSt_NhlD7olm7hFFAnHZcV-FMLCJti9CYdOR2zHZ7M')
    print('decode result:', result)

  def testTokenExpired(self):
    expires = datetime.datetime(2090, 10, 1, 1, 1, 1).timestamp()
    token = jwt.encode({'username': 'test', 'expires': expires}, secret, algorithm)
    self.assertEqual(isTokenExpired(token), False)

    expires = datetime.datetime(2000, 10, 1, 1, 1, 1).timestamp()
    token = jwt.encode({'username': 'test', 'expires': expires}, secret, algorithm)
    self.assertEqual(isTokenExpired(token), True)

  def testGenerateForgetPasswordToken(self):
    result = generateForgetPasswordToken('davidzhu20122014@gmail.com')
    print('result', result)
    # self.assertEqual(result.decode('ascii'), 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6InpodWVuMjAwMDBAMTYzLmNvbSIsImV4cGlyZXMiOjE2MDU5MTExNzAuOTcwMDk0fQ.526frrebmfuEABsQp-pjJ0eeZdXHcP2poOIrw2Zh-xc')
  
  def testDecodeForgetPassword(self):
    result = decodeJwt('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6InpodWVuMjAwMDBAMTYzLmNvbSIsImV4cGlyZXMiOjE2MDU5MTExNzAuOTcwMDk0fQ.526frrebmfuEABsQp-pjJ0eeZdXHcP2poOIrw2Zh-xc')  
    self.assertEqual(result['email'], 'zhuen20000@163.com')
