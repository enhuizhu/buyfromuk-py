import jwt
from datetime import datetime, timedelta


secret = '^mu~(YE(jG\e7(k&'
algorithm='HS256'

def generateJwt(username):
  now = datetime.now()
  # expires = datetime.datetime(now.year, now.month, now.day + 1, now.hour, now.minute, now.second)
  expires = now + timedelta(days=1)
  return jwt.encode({'username': username, 'expires': expires.timestamp()}, secret, algorithm=algorithm)

def decodeJwt(token):
  return jwt.decode(token, secret, algorithm=[algorithm])

def isTokenExpired(token):
  info = decodeJwt(token)
  now = datetime.now()
  return now.timestamp() > info['expires']

def generateForgetPasswordToken(email):
  now = datetime.now()
  expires = now + timedelta(minutes=20)
  return jwt.encode({'email': email, 'expires': expires.timestamp()}, secret, algorithm=algorithm)
