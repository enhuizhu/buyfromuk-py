from django.apps import AppConfig


class CustomersManagementConfig(AppConfig):
    name = 'customers_management'

# from django.contrib.admin.apps import AdminConfig

# class CustomersManagementConfig(AdminConfig):
#     defaunamelt_site = 'customers_management.admin.MyAdminSite'