from django.shortcuts import render
from django.http import HttpResponse
from customers_management.models import Customers, topUpHistory
from django.template.loader import get_template
from decimal import Decimal
from .models import CURRENCIES

# Create your views here.
def topup(request):
  if not request.user.is_authenticated:
    return HttpResponse('please login first')

  customerId = request.GET.get('id')
  customer = None

  if customerId:
    customer = Customers.objects.get(pk=customerId)

  postCurrencyValue = None
  
  if request.method == 'POST':
    postCurrencyValue = Decimal(request.POST.get('poundValue', 0))
    note = request.POST.get('note', '')
    currency = request.POST.get('currency', CURRENCIES[0][0])
    
    addMoneyToBalance(customer, postCurrencyValue, note, currency, request.user.username)
    customer.save()

  histories = topUpHistory.objects.filter(customer = customer).order_by('-created_date')

  t = get_template('customers/topup.html')
  
  html = t.render({
    'customer': customer,
    'histories': histories,
    'currencies': CURRENCIES,
  })

  return HttpResponse(html)

def addMoneyToBalance(customer, amount, note, currency, username):
  if currency == '0':
    customer.balance = customer.balance + amount
  else:
    customer.rmb_balance = customer.rmb_balance + amount
  
  topup = topUpHistory(
    customer = customer,
    value = amount,
    note = note,
    balance = customer.balance if currency == '0' else customer.rmb_balance,
    currency = currency,
    createdBy = username
  )
  
  customer.save()
  topup.save()