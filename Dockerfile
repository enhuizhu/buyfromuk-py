FROM python:3

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        postgresql-client \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app
RUN apt-get install -y --no-install-recommends default-libmysqlclient-dev
RUN pip install --upgrade pip
RUN pip install Django
RUN pip install mysqlclient
RUN pip install djangorestframework 
RUN pip install djangorestframework-jwt
RUN pip install django-cors-headers
RUN pip install passlib
RUN pip install pyjwt
RUN pip install stripe
RUN pip install django-fs-trumbowyg
Run pip install django-quilljs
Run pip install captcha
# COPY . .

EXPOSE 8000
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
