from django.contrib import admin
from django.utils.html import format_html

# Register your models here.
from .models import Order
from products.models import Product
import logging

logger = logging.getLogger(__name__)

class OrderAdmin(admin.ModelAdmin):
  search_fields = ('area', 'receiver', 'mobileNumber', 'status', 'customer__username')
  list_filter = ('status',)
  list_display = ('receiver', 'mobileNumber', 'area', 'address', 'status', 'products', 'totalFees', 'currency', 'customer', 'print')
  fields = ('status', 'parcelId', 'receiver', 'mobileNumber', 'area', 'address', 'personalId', 'paymentType', 'transactionId', 'orderDate', 'customer', 'products', 'totalFees', 'currency')
  readonly_fields = ('receiver', 'mobileNumber', 'area', 'address', 'personalId', 'paymentType', 'transactionId', 'orderDate', 'customer', 'products', 'totalFees', 'currency')
  
  def products(self, obj):
    content = ''
    sum = 0

    logger.info('extract product information')
    logger.info(obj.productInfo)
    
    for item in obj.productInfo:
      product = Product.objects.get(pk=item['id'])
      content += f'<div>{product.title} * {item["quantity"]}</div>'
      sum += product.price * item['quantity']  
    
    content += f'<div style="text-align:right">Total: &pound;{sum}</div>'
    # content = format_html('<h1>hello, the world.</h1>')
    return format_html(content)
  
  def print(self, obj):
    return format_html(f'<div><input type="button" class="button" style="padding: 5px;" onClick="window.open(\'/orders/print/?id={obj.id}\', name=\'blank\', \'width:400px, height=600px, titlebar=no, toolbar=no, status=no\')" value="Print"/></div>')

  def get_readonly_fields(self, request, obj=None):
    if obj and obj.status == '3':
      return self.readonly_fields + ('status',)
    return self.readonly_fields

  # def cancel(self, obj):
  #   if obj.status != '0':
  #     content = ''
  #   else:
  #     content = f'<div><input type="button" class="button" style="padding: 5px;" onClick="window.open(\'/orders/cancel/?id={obj.id}\', name=\'blank\', \'width:400px, height=600px, titlebar=no, toolbar=no, status=no\')" value="Cancel Order"/></div>'
  #   return format_html(content)




admin.site.register(Order, OrderAdmin)
