from django.db import models
from datetime import datetime
from django.utils import timezone
# from enum import Enum
from common.libs import ChoiceEnum
from customers_management.models import Customers, CURRENCIES
from customers_management.services import RefundService

# # within your models.Model class...
SYSTEM = 'System'

STATUS = (
  ('0', 'Receive Payment'),
  ('1', 'On Delivery'),
  ('2', 'Delivered'),
  ('3', 'Cancel Order'),
)

PAYMENT_TYPE = (
  ('0', 'Stripe'),
  ('1', 'Ali Pay'),
  ('2', 'WeChat Pay'),
  ('3', 'From Balance'),
)

# Create your models here.
class Order(models.Model):
  receiver = models.CharField(max_length=150)
  mobileNumber = models.CharField(max_length=26)
  area = models.CharField(max_length=200)
  parcelId= models.CharField(max_length=2000, default='', blank=True)
  address = models.TextField()
  personalId = models.CharField(max_length=250)
  paymentType = models.CharField(max_length=1, choices=PAYMENT_TYPE)
  currency = models.CharField(max_length=1, choices=CURRENCIES, default='0')
  transactionId = models.CharField(max_length=250)
  status = models.CharField(max_length=1, choices=STATUS)
  productInfo = models.JSONField()
  orderDate = models.DateTimeField('Order Time,', default=timezone.now)
  customer = models.ForeignKey(to=Customers, related_name='order', on_delete=models.CASCADE)
  totalFees = models.DecimalField(max_digits=9, decimal_places=2, default=0)

  def __init__(self, *args, **kwargs):
    super(Order, self).__init__(*args, **kwargs)
    self.__original_status = self.status

  def __str__(self):
    return self.receiver + ' ' + self.mobileNumber

  def save(self, force_insert=False, force_update=False, *args, **kwargs):
    if self.status != self.__original_status and self.status == '3':
      # from balance
      if self.paymentType == '3':
        RefundService.refundfromBalance(self, SYSTEM)
      elif self.paymentType == '0':
        RefundService.refundfromStrip(self, SYSTEM)

    super(Order, self).save(force_insert, force_update, *args, **kwargs)
    self.__original_status = self.status
