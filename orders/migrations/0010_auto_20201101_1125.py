# Generated by Django 3.1.1 on 2020-11-01 11:25

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0009_auto_20201029_2227'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='orderDate',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='Order Time,'),
        ),
    ]
