# Generated by Django 3.1.1 on 2020-10-29 22:27

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0008_auto_20201024_2153'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='totalFees',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=9),
        ),
        migrations.AlterField(
            model_name='order',
            name='orderDate',
            field=models.DateTimeField(default=datetime.datetime(2020, 10, 29, 22, 27, 28, 836408), verbose_name='Order Time,'),
        ),
        migrations.AlterField(
            model_name='order',
            name='paymentType',
            field=models.CharField(choices=[('0', 'Stripe'), ('1', 'Ali Pay'), ('2', 'WeChat Pay'), ('3', 'From Balance')], max_length=1),
        ),
    ]
