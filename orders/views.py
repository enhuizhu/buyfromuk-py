from django.shortcuts import render
from django.template.loader import get_template
# Create your views here.
from django.http import HttpResponse
from orders.models import Order, STATUS
from products.models import Product

def print(request):
  if not request.user.is_authenticated:
    return HttpResponse('please login first')

  orderId = request.GET.get('id')
  order = None
  
  if orderId:
    order = Order.objects.get(pk=orderId)
    order.status = STATUS[int(order.status)][1]
    sum = 0
    for info in order.productInfo:
      info["product"] = Product.objects.get(pk=info["id"])
      info["sum"] = info["product"].price * info["quantity"]
      sum += info["sum"]
    order.sum = sum

  t = get_template('orders/print.html')
  html = t.render({
    'order': order
  })
  
  return HttpResponse(html)
