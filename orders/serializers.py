from rest_framework import serializers
from .models import Order

class OrderSerializer(serializers.ModelSerializer):
  class Meta:
    model = Order
    field = (
      'id',
      'receiver',
      'area',
      'address',
      'personalId',
      'paymentType',
      'transactionId',
      'status',
      'productInfo',
      'orderDate',
    )
