from django.conf import settings

def getHostUrl():
  production = getattr(settings, 'PRODUCTION', True)

  if production:
    return "https://www.uk-haitao.com/"
  else:
    return "http://localhost/"