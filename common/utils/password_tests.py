from django.test import TestCase
from .password import encrypt_password, check_encrypted_password, make_user_password

class PasswordTestCase(TestCase):
  def testEncrptPassword(self):
    result = encrypt_password('test')
    print('result: ' + result)
    self.assertEqual(check_encrypted_password('test', result), True)
  