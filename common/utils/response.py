from django.http import HttpResponse
import json
from django.core.serializers.json import DjangoJSONEncoder


def jsonRes(msg, error = False, data = False):
  respone_data ={}

  if msg:
    respone_data['message'] = msg
  
  respone_data['error'] = error

  if data:
    respone_data['data'] = data

  return HttpResponse(json.dumps(respone_data, sort_keys=True, indent=2, cls=DjangoJSONEncoder), content_type='application/json')
