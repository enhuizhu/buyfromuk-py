from django.db import models
from django.utils import timezone
from datetime import datetime
# Create your models here.

class ExchangeRate(models.Model):
  name = models.CharField(max_length=155)
  rate = models.DecimalField(max_digits=9, decimal_places=2, default = 1)
  update_date = models.DateTimeField('Updated Date', default=datetime.now)
  
  def __str__(self):
    return self.name

