# Generated by Django 2.0.8 on 2020-09-06 16:56

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exchangeRate', '0002_auto_20200906_1649'),
    ]

    operations = [
        migrations.DeleteModel(
            name='ExchangeRate',
        ),
    ]
