# Generated by Django 3.1.1 on 2020-10-29 22:27

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exchangeRate', '0017_auto_20201024_2153'),
    ]

    operations = [
        migrations.AlterField(
            model_name='exchangerate',
            name='update_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 10, 29, 22, 27, 28, 806309), verbose_name='Updated Date'),
        ),
    ]
