from django.contrib import admin
from django.db import models
from django.http import HttpResponse
from django.urls import path
from orders.models import Order, PAYMENT_TYPE
from products.models import Product
import json
from django.forms.models import model_to_dict
from django.core.serializers.json import DjangoJSONEncoder
import logging

logger = logging.getLogger(__name__)
# Register your models here.
# my dummy model
class DummyModel(models.Model):
  class Meta:
    verbose_name_plural = 'Reports'
    app_label = 'reports'
    verbose_name= 'Reports'

class DummyModelAdmin(admin.ModelAdmin):
  model = DummyModel
  change_list_template = 'reports/index.html'

  def my_custom_view(self, request):
    return HttpResponse('<h1>Admin Custom View</h1>')


  def changelist_view(self, request, extra_context=None):
    extra_context = extra_context or {}
    products = Product.objects.all();    
    extra_context['products'] = products
    
    if request.method == 'POST':
      if request.POST.get('start_date') and request.POST.get('end_date'):
        extra_context['start_date'] = request.POST.get('start_date')
        extra_context['end_date'] = request.POST.get('end_date')
        filteredOrders = Order.objects.filter(orderDate__gte=request.POST.get('start_date'), orderDate__lte=request.POST.get('end_date')).order_by('-id')
        
        if request.POST.get('product'):
          extra_context['selectedProduct'] = int(request.POST.get('product'))
          newOrders = []
          for order in filteredOrders:
            for product in order.productInfo:
              if product['id'] == extra_context['selectedProduct']:
                newOrders.append(order)
                break
          filteredOrders = newOrders
        
        for order in filteredOrders:
          order.paymentType = PAYMENT_TYPE[int(order.paymentType)][1]
          

        extra_context['filteredOrders'] = filteredOrders
        extra_context['paymentType'] = PAYMENT_TYPE
        
    return super(DummyModelAdmin, self).changelist_view(request, extra_context)


  def get_urls(self):
    view_name = '{}_{}_changelist'.format(
      self.model._meta.app_label, self.model._meta.model_name)
    return [
      path('my_admin_path/', self.changelist_view, name=view_name),
    ]

admin.site.register(DummyModel, DummyModelAdmin)
