import json
import logging
from token import EQUAL

from django.http import HttpResponse
from rest_framework_jwt.settings import api_settings

logger = logging.getLogger(__name__)

class AttachUserInfoMiddleware(object):
  def __init__(self,  next_layer=None):
    self.get_response = next_layer

  def __call__(self, request):
    response = self.process_request(request)

    if response is None:
      response = self.get_response(request)
    
    return response

  def process_request(self, r):
    path = r.path_info

    if path != '/api/api-token-auth/' and hasattr(r, 'META') and 'HTTP_AUTHORIZATION' in r.META:
      logger.error('here it is')
      token = r.META['HTTP_AUTHORIZATION']
      token = token[4:]
      jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
      
      try:
        decodedPayload = jwt_decode_handler(token)
      except:
        logger.error('invalid token')
      else:
        logger.error(json.dumps(decodedPayload, indent=2))
        r.userInfo = decodedPayload



class corsMiddleware(object):
  def __init__(self,  next_layer=None):
    self.get_response = next_layer
   
  def __call__(self, request):
    response = self.process_response(request, self.get_response(request))
    
    return response
  
  def process_response(self, req, resp):
    logger.error('response here it is')
    resp["Access-Control-Allow-Origin"] = "*"
    return resp