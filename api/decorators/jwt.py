import logging
import json
from rest_framework_jwt.settings import api_settings
from django.http import HttpResponse
from customers_management.services.CustomerTokeService import decodeJwt, isTokenExpired
from common.utils.response import jsonRes
from common.utils.password import check_encrypted_password
from customers_management.models import Customers
from jwt import DecodeError

logger = logging.getLogger(__name__)

def isAuthrisedWithJWT(func):
  def func_wrapper(r):
    if 'META' in r and 'HTTP_AUTHORIZATION' in r.META:
      token = r.META['HTTP_AUTHORIZATION']
      token = token[4:]
      jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
      try:
        decodedPayload = jwt_decode_handler(token)
      except:
        return HttpResponse('invalid token')
      else:
        logger.error(json.dumps(decodedPayload, indent=2))
        return func(r)
    else:
      return func(r)
   
  return func_wrapper


def requireAuth(func):
  def func_wrapper(r):
    logger.info('headers:', r.META)
    if hasattr(r, 'META') and 'HTTP_AUTHORIZATION' in r.META:
      token = r.META['HTTP_AUTHORIZATION']
      info = {}

      try:
        if isTokenExpired(token):
          return jsonRes('token is expired, please refresh the token or login again', False)
        
        info = decodeJwt(token)
      except DecodeError:
        return jsonRes('invalid token', True)


      logger.info('token info', info)

      try:
        user = Customers.objects.get(username=info['username'])
        r.user = user
      except OSError:
        return jsonRes('user does not exist', False)

      if not user:
        return jsonRes('invalid token', False)

      return func(r)
    else:
      return jsonRes('please provide token in the header', False)

  return func_wrapper

