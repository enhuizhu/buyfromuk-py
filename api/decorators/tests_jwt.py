from django.test import TestCase
from .jwt import isAuthrisedWithJWT, requireAuth
from customers_management.services.CustomerTokeService import generateJwt, decodeJwt, isTokenExpired, secret, algorithm
import logging
import datetime
import jwt

logger = logging.getLogger(__name__)


class JWTTestCase(TestCase):
  # def test_isAuthrisedWithJWT(self):
  #   def fun(r):
  #     return r
    
  #   class Re():
  #     META =  {
  #       'HTTP_AUTHORIZATION':  'JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NTQ1Njk3MzMsImVtYWlsIjoiemh1ZW4yMDAwQDE2My5jb20iLCJ1c2VybmFtZSI6ImFkbWluIiwib3JpZ19pYXQiOjE1NTE5Nzc3MzMsInVzZXJfaWQiOjF9.pyzfQdRMR9lO4DISQngxX7EefJNOrUj1ct99IRlua1g'
  #     }
        
  #   re = Re()

  #   result = isAuthrisedWithJWT(fun)(re)

  #   self.assertEqual(1, 1)

  # def test_isAuthrisedWithJWTAndError(self):
  #   def fun(r):
  #     return r
    
  #   class Re():
  #     META =  {
  #       'HTTP_AUTHORIZATION':  'JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9'
  #     }
        
  #   re = Re()
  #   result = isAuthrisedWithJWT(fun)(re)
  #   self.assertEqual(1, 1)
  

  def testRequireAuth(func):
    def fun(r):
      return r
    
    expires = datetime.datetime(2000, 10, 1, 1, 1, 1).timestamp()
    token = jwt.encode({'username': 'test', 'expires': expires}, secret, algorithm)

    class Re():
      META =  {
        'HTTP_TOKEN': token
      }
    
    re = Re()
    result = requireAuth(fun)(re)
    print('require auth result:', result)

    expires = datetime.datetime(3090, 10, 1, 1, 1, 1).timestamp()
    token = jwt.encode({'username': 'lilisun', 'expires': expires}, secret, algorithm)

    class Re():
      META =  {
        'HTTP_TOKEN': token
      }
    
    re = Re()
    result = requireAuth(fun)(re)
    print('require auth result2:', result)        