from django.urls import path
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token
from .views import (products, 
  account, 
  exchangeRate, 
  customer, 
  stripePayment, 
  orders, 
  capcha, 
  alipayPayment,
  wechatPayment,
  topup,
  payment
)

urlpatterns = [
  path('categories/', products.categories.as_view(), name='catogory'),
  path('products/', products.allProducts.as_view(), name='all products'),
  path('exchange-rate/', exchangeRate.exChangeRateView.as_view(), name='all exchange rates'),
  path('category/<int:id>/', products.productsInCategory.as_view(), name='products in category'),
  path('product/description/<int:id>/', products.getDescrition),
  path('customer/create/', customer.createCustomer),
  path('customer/login/', customer.login),
  path('customer/profile/', customer.profile),
  path('customer/addressbook/', customer.addressbook),
  path('customer/balance-history/', customer.balanceHistory),
  path('customer/update-password/', customer.updatePasswordBaseOnToken),
  path('customer/send-user-forget-pasword-token/', customer.sendUserForgetPasswordToken),
  path('payment/stripe/', stripePayment.createStripePayment),
  path('payment/alipay/', alipayPayment.createAlipayPayment),
  path('payment/wechat/', wechatPayment.createWechatPayment),
  path('payment/topup/', topup.addMoneyToBalanceApi),
  path('payment/notify/', payment.receivePayment),
  path('order/createOrder/', orders.createOrder),
  path('order/all/', orders.getAllOrders),
  path('order/cancel/', orders.cancelOrder),
  path('account/changePassword/', account.changePassword),
  path('account/createUser/', account.createUser),
  path('account/userProfile/', account.userProfile),
  path('api-token-auth/', obtain_jwt_token),
  path('api-token-refresh/', refresh_jwt_token),
  path('api-token-verify/', verify_jwt_token),
  path('capcha/generate', capcha.create)
]
