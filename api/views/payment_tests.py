from django.test import TestCase
from .payment import (CATEGORY_MILK, 
  getTotalOfCustomsPrice, 
  getTotalWeight, 
  canItContainBoxOfFourAndBoxOfSix,
  getDeliveryFees, 
  getDeliveryFeesForMilk)

class Category:
  def __init__(self, name):
    self.name = name

class Product:
  def __init__(self, price, customs_price, post_weight, category):
    self.price = price
    self.customs_price = customs_price
    self.post_weight = post_weight
    self.category = category

class PaymentTestCase(TestCase):
  def testGetTotalOfCustomsPrice(self):
    allProducts = []
    product = Product(1, 96, 800, Category(CATEGORY_MILK))
    product.quantity = 4
    allProducts.append(product)
    self.assertEqual(getTotalOfCustomsPrice(allProducts), 384)
 
  def testGetTotalWeight(self):
    allProducts = []
    product = Product(1, 96, 800, Category(CATEGORY_MILK))
    product.quantity = 4
    allProducts.append(product)
    self.assertEqual(getTotalWeight(allProducts), 3200)

  def testCanItContainBoxOfFourAndBoxOfSix(self):
    self.assertEqual(canItContainBoxOfFourAndBoxOfSix(384), False)
    self.assertEqual(canItContainBoxOfFourAndBoxOfSix(480), False)
    self.assertEqual(canItContainBoxOfFourAndBoxOfSix(576), True)

  def testGetDeliveryFeesForMilk(self):
    allProducts = []
    product = Product(1, 96, 800, Category(CATEGORY_MILK))
    product.quantity = 4
    allProducts.append(product)
    self.assertEqual(getDeliveryFeesForMilk(allProducts), 15)
    product2 = Product(1, 96, 800, Category(CATEGORY_MILK))
    product2.quantity = 2
    allProducts.append(product2)
    self.assertEqual(getDeliveryFeesForMilk(allProducts), 30)
    product3 = Product(1, 48, 800, Category(CATEGORY_MILK))
    product3.quantity = 8
    allProducts.append(product3)
    self.assertEqual(getDeliveryFeesForMilk(allProducts), 45)

  def testGetDeliveryFeesWhenMixMilkAndOthers(self):
    allProducts = []
    product = Product(1, 96, 800, Category(CATEGORY_MILK))
    product.quantity = 4
    allProducts.append(product)

    product2 = Product(1, 96, 800, Category('others'))
    product2.quantity = 4
    allProducts.append(product2)

    try:
      getDeliveryFees(allProducts)
    except Exception:
      pass
  
  def testGetDeliveryFeesWhenOnlyContainOthers(self):
    allProducts = []
    product2 = Product(1, 96, 1000, Category('others'))
    product2.quantity = 11
    allProducts.append(product2)

    try:
     getDeliveryFees(allProducts)
    except Exception:
     pass 

  def testGetDeliveryFeesWhenOnlyContainMilk(self):
    allProducts = []
    product = Product(1, 96, 1000, Category(CATEGORY_MILK))
    product.quantity = 4
    allProducts.append(product)

    self.assertEqual(getDeliveryFees(allProducts), 15)
    product.quantity = 6
    self.assertEqual(getDeliveryFees(allProducts), 30)

    product.quantity = 10
    self.assertEqual(getDeliveryFees(allProducts), 45)


    



