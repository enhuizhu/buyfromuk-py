from common.utils.response import jsonRes
import json
import logging
from orders.models import Order, STATUS, PAYMENT_TYPE
from api.decorators.jwt import requireAuth
from django.forms.models import model_to_dict
from customers_management.models import addressBook, topUpHistory
from products.models import Product
from .payment import calculateAmount
from customers_management.views import addMoneyToBalance
from exchangeRate.models import ExchangeRate
from django.core.paginator import Paginator
from pprint import pprint
from customers_management.services import RefundService

logger = logging.getLogger(__name__)

@requireAuth
def createOrder(request):
  if request.method == 'POST':
    try:
      rowData = json.loads(request.body.decode('utf-8'))
      
      #check if need to check if we need to save the infomation to address book
      # logger.info('save the address' + str(rowData['receiverInfo']['saveTheAddress']))
      
      if 'saveTheAddress' in rowData['receiverInfo'] and rowData['receiverInfo']['saveTheAddress']:
        newAddressBook = addressBook(
          customer = request.user,
          name = rowData['receiverInfo']['name'],
          mobileNumber = rowData['receiverInfo']['mobileNumber'],
          area = rowData['receiverInfo']['area'],
          address = rowData['receiverInfo']['address'],
          personalId = rowData['receiverInfo']['personalId'],
        )
        newAddressBook.save()
      
      totalFees = calculateAmount(rowData)
      exchangeRate = ExchangeRate.objects.first().rate

      #need to find the user
      order = Order(
        receiver = rowData['receiverInfo']['name'],
        mobileNumber = rowData['receiverInfo']['mobileNumber'],
        area = rowData['receiverInfo']['area'],
        address = rowData['receiverInfo']['address'],
        personalId = rowData['receiverInfo']['personalId'],
        paymentType = PAYMENT_TYPE[rowData['payment']['type']][0],
        transactionId = rowData['payment']['transactionId'],
        status = STATUS[0][0],
        currency = '0' if 'currency' not in rowData else rowData['currency'],
        productInfo = rowData['products'],
        customer = request.user,
        totalFees = totalFees if 'currency' not in rowData else (totalFees if rowData['currency'] == '0' else totalFees * exchangeRate)
      )

      if (rowData['payment']['type'] == 3):
        reduceBalance(request.user, rowData['products'], totalFees, rowData['currency'], exchangeRate)

      order.save()
      return jsonRes('已经成功单!', False)

    except Exception as e:
      return jsonRes(str(e), True)
  
  return jsonRes(False, 'unsupported request method.')

@requireAuth
def getAllOrders(request):
  newOrders = []
  orders = Order.objects.filter(customer=request.user).order_by('-id')
  countPerPage = 15
  pageNumber = int(request.GET.get('page_number', 1))
  paginator = Paginator(orders, countPerPage)
  pageOrders = paginator.page(pageNumber)

  for order in pageOrders:
    order.status = STATUS[int(order.status)][1]
    newOrders.append(model_to_dict(order))
  
  response = {
    'orders': newOrders,
    'totalPages': paginator.num_pages,
    'count': paginator.count,
    'currentPage': pageNumber,
    'recordsPerPage': countPerPage
  }
  
  return jsonRes(False, False, response)

@requireAuth
def cancelOrder(request):
  id = request.GET.get('id')
  order = Order.objects.get(pk=id)
  
  try:
    if order.status == '0':
      order.status = '3'
      order.save()
      return jsonRes('退款成功！', False)
  except Exception as e:
    return jsonRes(e, True)

def reduceBalance(user, productInfo, totalFees, currency, exchangeRate):
  finalTotalFees = totalFees if currency == '0' else totalFees * exchangeRate
  finalBalance = user.balance if currency == '0' else user.rmb_balance

  if finalTotalFees > finalBalance:
    raise Exception('余额不足')

  # user.balance = user.balance - totalFees  
  addMoneyToBalance(user, -finalTotalFees, 'buy products:' + json.dumps(productInfo), currency, user.username)
  # user.save()
