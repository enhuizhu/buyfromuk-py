from captcha.image import ImageCaptcha
from django.http import HttpResponse
import random

image = ImageCaptcha()

def create(request):
  str = generateRadomStr()
  data = image.generate(str)
  request.session['captcha'] = str
  return HttpResponse(data, content_type="image/jpeg")

def generateRadomStr(): 
  str = '0123456789'
  index1 = random.randint(0, len(str) - 1)
  index2 = random.randint(0, len(str) - 1)
  index3 = random.randint(0, len(str) - 1)
  index4 = random.randint(0, len(str) - 1)
  return f'{str[index1]}{str[index2]}{str[index3]}{str[index4]}'
