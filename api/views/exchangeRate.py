from rest_framework import generics
from exchangeRate.models import ExchangeRate
from rest_framework import serializers
from exchangeRate.serializers import ExchangeRateSerializer


class exChangeRateView(generics.ListAPIView):
  queryset = ExchangeRate.objects.all()
  model = ExchangeRate
  serializer_class = ExchangeRateSerializer 
  
