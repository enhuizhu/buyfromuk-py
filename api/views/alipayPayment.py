from .payment import createPayment

def createAlipayPayment(request):
  return createPayment(request, 'alipay')
  