from common.utils.response import jsonRes
import stripe 
import json
import logging
from products.models import Product
from .payment import createPayment
from django.conf import settings

stripe.api_key = settings.STRIPE_API_KEY

logger = logging.getLogger(__name__)

def createStripePayment(request):
  return createPayment(request)

def cancelPayment(order):
  refund = stripe.Refund.create(
    payment_intent=order.transactionId,
  )
  