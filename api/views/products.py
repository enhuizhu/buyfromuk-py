from django.shortcuts import render
from django.http import HttpResponse
from django.core import serializers
from rest_framework import generics
from rest_framework.response import Response
from products.serializers import CategorySerializer, ProductSerializer
from products.models import Category, Product, Group_images
from common.utils.response import jsonRes
import json

class categories(generics.ListAPIView):
  queryset = Category.objects.all()
  model = Category
  serializer_class = CategorySerializer 

class productsInCategory(generics.ListAPIView):
  queryset = ''
  model = Product
  serializer_class = ProductSerializer

  def list(self, request, id):
    queryset =  Product.objects.filter(category__id=id)
    serializer = ProductSerializer(queryset, many=True)
    return Response(serializer.data)

class allProducts(generics.ListAPIView):
  queryset = Product.objects.all()
  model = Product
  serializer_class = ProductSerializer
 
def getDescrition(request, id):
  product = Product.objects.get(pk=id)
  return jsonRes(product.description)