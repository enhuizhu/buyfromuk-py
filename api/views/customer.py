import json
import logging
from customers_management.models import Customers
from django.db.models import Q
from rest_framework.response import Response
from common.utils.response import jsonRes
from common.utils.password import encrypt_password, check_encrypted_password
from customers_management.services.CustomerTokeService import (generateJwt, 
  decodeJwt, 
  isTokenExpired,
  generateForgetPasswordToken
)
from customers_management.models import addressBook, topUpHistory
import sys
from api.decorators.jwt import requireAuth
from django.forms.models import model_to_dict
from django.core.paginator import Paginator
from services.file_service import loadForgetPasswordTemplate
from django.http import HttpResponse
from services.mail_service import send
from django.conf import settings
from datetime import datetime, timedelta

logger = logging.getLogger(__name__)

def createCustomer(request):
  if request.method == 'POST':
    rawData = json.loads(request.body.decode('utf-8'))
    
    try:
      if rawData['captcha'] != request.session['captcha']:
        return jsonRes('captcha 错误', True)

      if Customers.objects.filter(Q(username=rawData['username']) | Q(email=rawData['email'])).count() > 0:
        msg = rawData['username'] + '或' + rawData['email'] + '已经注册过了！'
        return jsonRes(msg, True)
      
      result =  Customers(
        username = rawData['username'], 
        email = rawData['email'], 
        password = encrypt_password(rawData['password']),
        firstName = rawData['firstName'],
        lastName = rawData['lastName'],
        mobile = rawData['mobile']
      )

      result.save()

      msg = '{0} 已经成功注册！'.format(result)
      
      return jsonRes(msg, False)
    except OSError:
      msg = 'unexpected error!'
      return jsonRes(OSError, True)

  return jsonRes(False, 'unsupported request method')


def login(request):
  if request.method == 'POST':
    rawData = json.loads(request.body.decode('utf-8'))

    try:
      # check if the captcha is correct
      if rawData['captcha'] != request.session['captcha']:
          return jsonRes('captcha 错误', True)

      user = Customers.objects.get(username=rawData['username'])
      
      if user:
        print('username is:', generateJwt(user.username))
        password = rawData['password']

        if not check_encrypted_password(password, user.password):
          return jsonRes('密码错误', True)

        return jsonRes(generateJwt(user.username).decode('utf-8'), False)
      else:
        return jsonRes('用户名错误', True)
    except Exception as e:
      return jsonRes(str(e), True)    

  return jsonRes('unsupported request method', True)

@requireAuth
def profile(request):
  token = request.META['HTTP_AUTHORIZATION']
  info = decodeJwt(token)
  user = Customers.objects.get(username=info['username'])
  
  uesrinfo = {
    'username': user.username,
    'firstName': user.firstName,
    'lastName': user.lastName,
    'email': user.email,
    'mobile': user.mobile,
    'balance': user.balance,
    'rmb_balance': user.rmb_balance,
  }

  return jsonRes(None, False, uesrinfo)

@requireAuth
def addressbook(request):
  newAddressbooks = []
  addressbooks = addressBook.objects.filter(customer=request.user)

  for addressbook in addressbooks:
    newAddressbooks.append(model_to_dict(addressbook))
  
  return jsonRes(False, False, newAddressbooks)

@requireAuth
def balanceHistory(request):
  newHistories = []
  histories = topUpHistory.objects.filter(customer=request.user).order_by('-created_date')
  countPerPage = 15
  pageNumber = int(request.GET.get('page_number', 1))
  paginator = Paginator(histories, countPerPage)
  pageHistories = paginator.page(pageNumber)

  for history in pageHistories:
    newHistories.append(model_to_dict(history))

  response = {
    'histories': newHistories,
    'totalPages': paginator.num_pages,
    'count': paginator.count,
    'currentPage': pageNumber,
    'recordsPerPage': countPerPage
  }

  return jsonRes(False, False, response)


def updatePasswordBaseOnToken(request):
  if request.method == 'POST':
    rowData = json.loads(request.body.decode('utf-8'))
    token = rowData['token']

    # should check if the token expired or not
    if isTokenExpired(token):
      return jsonRes('token has been expired', True)
      
    userInfo = decodeJwt(token)

    try:
      customer = Customers.objects.get(email=userInfo['email'])
    except Customers.DoesNotExist:
      return jsonRes('user does not exist', False)

    try:
      customer.password = encrypt_password(rowData['password'])
      customer.save()
      return jsonRes('密码已经更新成功！')
    except Exception: 
      return jsonRes(Exception, True)
  else:
    return jsonRes('unsupported method', False)

def sendUserForgetPasswordToken(request):
  if request.method == 'POST':
    rawData = json.loads(request.body.decode('utf-8'))
    email = rawData['email']
    production = getattr(settings, 'PRODUCTION', True)
    
    try:
      customer = Customers.objects.get(email=email)
      token = generateForgetPasswordToken(email)
      template = loadForgetPasswordTemplate()
      url = "https://www.uk-haitao.com/" if production else "http://localhost:3000/"
      resetPassLink = url + "reset-password/" + token.decode('ascii')
      template = template.replace("%name", customer.username)
      template = template.replace("%link", resetPassLink)
      send('重置密码', template, email, customer.username)
      return jsonRes('重置密码的邮件已经发到您的邮箱，请查收并且在十分钟内重设密码，否则链接会失效。', False)
    except Customers.DoesNotExist:
      return jsonRes('Email does not exist', False)

  else:
    return jsonRes('unsupported method', False)

