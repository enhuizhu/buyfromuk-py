from api.decorators.jwt import requireAuth
from customers_management.views import addMoneyToBalance
from common.utils.response import jsonRes
import json

@requireAuth
def addMoneyToBalanceApi(request):
  if request.method == 'POST':
    try:
      rawData = json.loads(request.body.decode('utf-8'))
      addMoneyToBalance(request.user, rawData['amount'], 'top up', rawData['currency'], request.user.username)
      return jsonRes('已经成功充值！', False)
    except Exception as e:
      return jsonRes(str(e), True)
  
  return jsonRes('unsupported request method', True)
