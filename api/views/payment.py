from products.models import Product
from common.utils.response import jsonRes
import json
import stripe
import logging
from functools import reduce
import math
from exchangeRate.models import ExchangeRate
from django.conf import settings
import urllib.parse;
import requests
import hashlib;

# stripe.api_key = "sk_test_wV3lcW3bYUrDnWaMf2RPazPq"
logger = logging.getLogger(__name__)

CATEGORY_MILK = "英国奶粉"
BottleMilkCustomPrice = 96
BoxOfFourMilks = BottleMilkCustomPrice * 4
boxOfSixMilks = BottleMilkCustomPrice * 6
feesForBoxOfFour = 15
feesForBoxOfSix = 30

def receivePayment(request):
  rawData = json.loads(request.body.decode('utf-8'))
  return jsonRes(None, False, rawData)

def createqueryString(dic):
  result = ''
  
  for k,v in dic.items():
    result += f'{k}={v}&'
  
  return result[0:-1]

def createPayment(request, method = None):
  if request.method == 'POST':
    try:
      logger.info('the payment method is:' + str(method))
      rawData = json.loads(request.body.decode('utf-8'))
      # return jsonRes(str(calculateAmount(rawData)), False)
      logger.info('raw data' + str(rawData))
      amount = calculateAmount(rawData)

      logger.info('amount is:' + str(amount))

      if method == 'alipay' or method == 'wechat':
        clientId = getattr(settings, 'FUSIONPAY_CLIENT_ID')
        apiKey = getattr(settings, 'FUSIONPAY_API_KEY')
        fusionpayUrl = getattr(settings, 'FUSIONPAY_URL')

        logger.info('clientId:' + clientId)
        logger.info('apiKey:' + apiKey)
        logger.info('fusionpayUrl:' + fusionpayUrl)

        
        instant = {
          "amount": amount,
          "client_id": clientId,
          "currency": "GBP",
          "notify_url": "https://uk-haitao.com:7000/payment/notify",
        }
        
        queryString = createqueryString(instant)
        sign = hashlib.md5((queryString + apiKey).encode()) 
        instant.update({"sign": sign.hexdigest()})
        fusionEndPoint = f'{fusionpayUrl}/api/v2/payment/qrcode/{method}'
        logger.info('fusionEndpoint:' + fusionEndPoint)
        # logger.info('data is:' + json.dumps(instant))
        r = requests.post(fusionEndPoint, data = instant)
        logger.info(r.request)
        return jsonRes(None, False, r.text)
      else:
        # amount = calculateAmount(rawData)
        exchangeRate = ExchangeRate.objects.first().rate

        if 'currency' in rawData and rawData['currency'] == '1':
          amount *= exchangeRate

        intent = stripe.PaymentIntent.create(
          amount = int(100 * amount),
          currency =  'cny' if 'currency' in rawData and rawData['currency'] == '1' else 'gbp',
          description =  'top up' if 'amount' in rawData else 'buy milk online'
        )

      return jsonRes(None, False, {'clientSecret': intent['client_secret']})
    except Exception as e:
      return jsonRes(str(e), True)
  
  return jsonRes('unsupported request method', True)

def calculateAmount(rawData):
  if 'amount' in rawData:
    return rawData['amount']

  sum = 0
  allProducts = []
  
  for productInfo in rawData['products']:
    product = Product.objects.get(pk=productInfo['id'])
    product.quantity = productInfo['quantity']
    allProducts.append(product)
    sum += productInfo['quantity'] * product.price
  
  deliveryFees = getDeliveryFees(allProducts)

  return sum + deliveryFees

def containsOthers(allProducts):
  return any(product.category.name != CATEGORY_MILK for product in allProducts)

def containsMilk(allProducts):
  return any(product.category.name == CATEGORY_MILK for product in allProducts)

def mixMilkAndOthers(allProducts):
  return containsOthers(allProducts) and containsMilk(allProducts)

def getDeliveryFeesForMilk(allProducts):
  if not isTheTotalQualityOfMilkValid(allProducts):
    raise Exception('总的奶粉数必须是4或者6的倍数')
  
  totalOfCustomPrice = getTotalOfCustomsPrice(allProducts)
  scaledTotal = totalOfCustomPrice / BottleMilkCustomPrice

  if scaledTotal % 4 == 0:
    numberOfBoxFour = scaledTotal / 4
    return feesForBoxOfFour * numberOfBoxFour

  numberOfBoxFour = (scaledTotal - 6) / 4
  return numberOfBoxFour * feesForBoxOfFour + feesForBoxOfSix  

def getDeliveryFeesForOtherCategory(allProducts):
  totalWeight = getTotalWeight(allProducts) / 1000
  if totalWeight > 10:
    raise Exception('总重量不能超过十公斤')

  if totalWeight < 1:
    return 10
  
  return 10 + math.ceil(totalWeight - 1) * 2

def getTotalWeight(allProducts):
  return reduce((lambda a, c: a + c.post_weight * c.quantity), allProducts, 0)

def isTheTotalQualityOfMilkValid(allProducts):
  totalOfCustomPrice = getTotalOfCustomsPrice(allProducts)

  return (totalOfCustomPrice % BoxOfFourMilks == 0 
    or totalOfCustomPrice % boxOfSixMilks == 0
    or canItContainBoxOfFourAndBoxOfSix(totalOfCustomPrice))

def getTotalOfCustomsPrice(allProducts):
  return reduce((lambda a, c: a + c.customs_price * c.quantity), allProducts, 0)

def canItContainBoxOfFourAndBoxOfSix(totalOfCustomPrice):
  if totalOfCustomPrice % BottleMilkCustomPrice != 0:
    return False
  
  scaledTotal = totalOfCustomPrice / 96
  mod4 = scaledTotal % 4

  return mod4 == 2 and scaledTotal > 4

def getDeliveryFees(allProducts):
  if mixMilkAndOthers(allProducts):
    raise Exception('奶粉不能和杂货混在一下')

  if containsOthers(allProducts):
    return getDeliveryFeesForOtherCategory(allProducts)

  if containsMilk(allProducts):
    return getDeliveryFeesForMilk(allProducts)
