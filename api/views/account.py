import json
import logging

from api.decorators.jwt import isAuthrisedWithJWT
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse
from rest_framework.request import Request
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework_jwt.serializers import VerifyJSONWebTokenSerializer
from rest_framework_jwt.settings import api_settings
from django.db.models import Q
from common.utils.response import jsonRes
from customers_management.services.CustomerTokeService import isTokenExpired, decodeJwt


logger = logging.getLogger(__name__)

# @isAuthrisedWithJWT
def changePassword(request):
  if request.method == 'POST':
    response_data = {}
    rawData = json.loads(request.body.decode('utf-8'))

    if hasattr(request, 'userInfo'):
      logger.error(request.userInfo)
      # should find the user
      user = User.objects.get(username=request.userInfo['username'])
      valid = user.check_password(rawData['old_password'])
      logger.error(request.userInfo['username'])
      if not valid:
        return jsonRes('password is wrong', True)
      else:
        user.set_password(rawData[''])
        return jsonRes('password has been updated', False)

    return jsonRes('invalid user token', True)


def userProfile(request):
  if hasattr(request, 'userInfo'):
    return jsonRes(False, False, request.userInfo)
  
  return jsonRes('invalid user token', True)


def createUser(request):
  if request.method == 'POST':
    rawData = json.loads(request.body.decode('utf-8'))
    # should check if the username or email already exist
    
    try:
      if User.objects.filter(Q(username=rawData['username']) | Q(email=rawData['email'])).count() > 0:
        msg = rawData['username'] + ' or ' + rawData['email'] + ' is already registered.'
        return jsonRes(msg, True)
      
      result =  User.objects.create_user(username=rawData['username'], email=rawData['email'], password=rawData['password'])
      msg = '{0} has been created successfully.'.format(result)
      return jsonRes(msg, False)
    except:
      msg = 'unexpected error!'
      return jsonRes(msg, True)



