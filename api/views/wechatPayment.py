from .payment import createPayment

def createWechatPayment(request):
  return createPayment(request, 'wechat')
  