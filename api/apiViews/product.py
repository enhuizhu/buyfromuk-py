from products.serializers import CategorySerializer, ProductSerializer
from products.models import Category, Product, Group_images
from rest_framework.response import Response

class productsInCategory(generics.ListAPIView):
  queryset = ''
  model = Product
  serializer_class = ProductSerializer

  def list(self, request, id):
    queryset =  Product.objects.filter(category__id=id)
    serializer = ProductSerializer(queryset, many=True)
    return Response(serializer.data)