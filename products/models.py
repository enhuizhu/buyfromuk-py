from django.db import models
from django.utils import timezone
from quilljs.fields import RichTextField
# Create your models here.

class Category(models.Model):
  name = models.CharField(max_length=200)
  created_date = models.DateTimeField('date created', default=timezone.now)
  slug = models.SlugField()
  parent = models.ForeignKey('self', blank=True, null=True ,related_name='children', on_delete=models.CASCADE)

  class Meta:
    unique_together = ('slug', 'parent',)   
    verbose_name_plural = "categories"       
                                            
  def __str__(self):                           
    full_path = [self.name]                 
    k = self.parent                          

    while k is not None:
        full_path.append(k.name)
        k = k.parent

    return ' -> '.join(full_path[::-1])


class Product(models.Model):
  title = models.CharField(max_length=200)
  # description = models.TextField()
  description = RichTextField()
  sku = models.CharField(max_length=200, default='')
  post_weight = models.DecimalField(max_digits=9, decimal_places=2, default=0)
  parcel_tax = models.DecimalField(max_digits=9, decimal_places=2, default=0)
  individually_packaged = models.BooleanField(default = False)
  purchase_limit_per_order = models.BooleanField(default = False)
  specification = models.CharField(max_length=200, default='')
  price = models.DecimalField(max_digits=9, decimal_places=2, default=0)
  customs_price = models.DecimalField(max_digits=9, decimal_places=2, default=0)
  post_rule = models.TextField(default='')
  featureImgUrl = models.FileField()
  category = models.ForeignKey('Category', on_delete=models.CASCADE)
  brand = models.ForeignKey('Brand', on_delete=models.CASCADE)
  created_date = models.DateTimeField('date created', default=timezone.now)
  
  def __str__(self):
    return self.title

class Group_images(models.Model):
  product = models.ForeignKey(to=Product, related_name="group_images", on_delete=models.CASCADE)
  url  = models.FileField()
  created_date = models.DateTimeField('date created', default=timezone.now)


class Brand(models.Model):
  title = models.CharField(max_length=200)
  description = models.TextField()

  def __str__(self):
    return self.title