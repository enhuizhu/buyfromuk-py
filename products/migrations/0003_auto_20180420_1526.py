# Generated by Django 2.0.4 on 2018-04-20 15:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0002_auto_20180420_1409'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='featureImgUrl',
            field=models.FileField(upload_to='uploads'),
        ),
    ]
