# Generated by Django 2.0.8 on 2020-09-05 20:12

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0019_auto_20200905_1537'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='individually_packaged',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='product',
            name='parcel_tax',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=9),
        ),
        migrations.AddField(
            model_name='product',
            name='post_weight',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=9),
        ),
        migrations.AddField(
            model_name='product',
            name='purchase_limit_per_order',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='product',
            name='sku',
            field=models.CharField(default='', max_length=200),
        ),
        migrations.AddField(
            model_name='product',
            name='specification',
            field=models.CharField(default='', max_length=200),
        ),
        migrations.AlterField(
            model_name='category',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 9, 5, 20, 12, 56, 329512, tzinfo=utc), verbose_name='date created'),
        ),
        migrations.AlterField(
            model_name='group_images',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 9, 5, 20, 12, 56, 356898, tzinfo=utc), verbose_name='date created'),
        ),
        migrations.AlterField(
            model_name='product',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 9, 5, 20, 12, 56, 355796, tzinfo=utc), verbose_name='date created'),
        ),
    ]
