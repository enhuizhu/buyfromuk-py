from django.contrib import admin

# Register your models here.
from .models import Category, Product, Group_images, Brand
from quilljs.admin import QuillAdmin

class GroupImagesInline(admin.StackedInline):
  model = Group_images
  extra = 0

class ProductAdmin(QuillAdmin):
  inlines = [GroupImagesInline]
  list_display = ('title', 'category', 'brand')

admin.site.register(Category)
admin.site.register(Product, ProductAdmin)
admin.site.register(Brand)

