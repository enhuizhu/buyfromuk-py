from rest_framework import serializers
from .models import Category, Product, Group_images, Brand
from buyfromuk.settings import MEDIA_URL

class CategorySerializer(serializers.ModelSerializer):
  class Meta:
    model = Category
    fields = ('id', 'name', 'parent')

class ProductSerializer(serializers.ModelSerializer):
  gimages = serializers.SerializerMethodField('get_group_images')
  brandName = serializers.SerializerMethodField('get_brand')
  categoryName = serializers.SerializerMethodField('get_category')
  featureImgUrl = serializers.SerializerMethodField('get_feature_image')

  class Meta:
    model = Product
    fields = (
      'id',
      'title', 
      # 'description', 
      'price',
      'featureImgUrl', 
      'brandName', 
      'categoryName',
      'sku',
      'post_weight',
      'parcel_tax',
      'individually_packaged',
      'purchase_limit_per_order',
      'specification',
      'post_rule',
      'customs_price',
      'gimages',
    )
  
  def get_feature_image(self, obj): 
    return '/media/' + obj.featureImgUrl.name

  def get_group_images(self, obj):
    results = []
    images = Group_images.objects.filter(product=obj.id)
    for img in images:
      results.append(MEDIA_URL + img.url.name)
    return results
  
  def get_brand(self, obj):
    return obj.brand.title
  
  def get_category(self, obj):
    return obj.category.name